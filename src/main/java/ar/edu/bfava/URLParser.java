package ar.edu.bfava;

import java.io.BufferedWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

public class URLParser extends Thread {

	private String urlString;

	private static Logger logger = Logger.getLogger(URLParser.class);

	private Set<String> matches = new HashSet<String>();

	final static String EXTENSION = ".txt";

	public URLParser(final String url) {
		this.urlString = url;
	}

	@Override
	public void run() {
		logger.info("Parsing url content for[" + urlString + "]");
		try {
			final String content = getUrlContentAsString();
			matches = findPatterns(content);
			writeResultsToFile(matches);
		} catch (Exception e) {
			logger.error("Error parsing file [" + urlString + "]", e);
		}
		logger.info("Finished parser for[" + urlString + "]");
	}

	public String getUrlContentAsString() {
		// Create URI and URL objects.
		String content = null;
		try {
			StringBuffer sb = new StringBuffer();
			URL url = new URL(urlString);

			Scanner scanner = new Scanner(url.openStream());
			while (scanner.hasNextLine()) {
				sb.append(scanner.nextLine());
			}

			scanner.close();
			content = sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return content;
	}

	public Set<String> findPatterns(final String content) {
		Set<String> matches = new HashSet<String>();
		for (final URLPattern urlPattern : URLPattern.values()) {
			final Pattern p = Pattern.compile(urlPattern.getPattern());
			final Matcher matcher = p.matcher(content);

			while (matcher.find()) {
				String result = matcher.group();
				matches.add(result);
				logger.debug("New match for [" + urlString + ", " + result + "]");
			}
		}
		return matches;
	}

	private void writeResultsToFile(final Set<String> matches) throws Exception {
		final String outputFileName = getDomainName(this.urlString);
		Path path = Paths.get(outputFileName + EXTENSION);
		try (BufferedWriter writer = Files.newBufferedWriter(path)) {
			for (String match : matches) {
				writer.write(match + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String getDomainName(final String url) throws URISyntaxException {
		final URI uri = new URI(url);
		final String domain = uri.getHost();
		return domain.startsWith("www.") ? domain.substring(4) : domain;
	}

	public Set<String> getMatches() {
		return matches;
	}
}
