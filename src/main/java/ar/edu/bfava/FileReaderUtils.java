package ar.edu.bfava;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;

public class FileReaderUtils {

	private static Logger logger = Logger.getLogger(FileReaderUtils.class);

	/**
	 * Returns fileContent lines on a List
	 * 
	 * @param filePath
	 * @return
	 */
	public static List<String> getUrls(final String filePath) {

		List<String> urls = new ArrayList<String>();

		try (Scanner scanner = new Scanner(new File(filePath))) {
			while (scanner.hasNext()) {
				urls.add(scanner.nextLine());
			}
		} catch (IOException e) {
			// This should be changed for logger.error("Error trying to read from file", e.getMessage());
			logger.error("Error trying to read from file, ", e);
		}
		return urls;
	}
}
