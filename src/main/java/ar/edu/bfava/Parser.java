package ar.edu.bfava;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class Parser {

	private static Logger logger = Logger.getLogger(Parser.class);

	public static void main(String[] args) {
		if (args.length == 1) {
			final String urlFilePath = args[0];

			if (!StringUtils.isBlank(urlFilePath)) {

				logger.info("UrlPath found parsing file [" + urlFilePath + "]");

				// Get urls from file
				final List<String> urls = FileReaderUtils.getUrls(urlFilePath);

				// Process each url with different thread.
				urls.forEach(url -> {
					final URLParser parser = new URLParser(url);
					parser.start();
				});
			}
		} else {
			logger.warn("No input file loaded, application finished");
		}
		logger.info("UrlPath found parsing finished OK ");
	}
}
