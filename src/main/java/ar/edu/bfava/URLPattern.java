package ar.edu.bfava;

public enum URLPattern {
	TWITTER_USER("(?<=^|(?<=[^a-zA-Z0-9-_\\.]))@([A-Za-z]+[A-Za-z0-9_]+)");

	private String pattern;

	private URLPattern(final String pattern) {
		this.pattern = pattern;
	}

	public String getPattern() {
		return pattern;
	}
}
